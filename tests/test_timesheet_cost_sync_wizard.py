# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest

import doctest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite
from trytond.tests.test_tryton import doctest_teardown, doctest_checker


class TimesheetCostSyncWizardTestCase(ModuleTestCase):
    """Test Timesheet Cost Sync Wizard module"""
    module = 'timesheet_cost_sync_wizard'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TimesheetCostSyncWizardTestCase))
    suite.addTests(doctest.DocFileSuite(
            'scenario_sync_cost.rst',
            tearDown=doctest_teardown, encoding='utf-8',
            checker=doctest_checker,
            optionflags=doctest.REPORT_ONLY_FIRST_FAILURE))
    return suite
